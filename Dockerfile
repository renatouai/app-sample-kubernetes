FROM node:8

WORKDIR /APP

COPY . .

RUN npm install

EXPOSE 80
EXPOSE 8080

CMD node index.js
