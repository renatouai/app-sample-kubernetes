const express = require('express')
const app = express()

app.get('/', (req, res) => res.send('Hello World 4'));

// On localhost:3000/welcome
app.get('/welcome', function (req, res) {
    res.send('<b>Hello</b> welcome to my http server made with express');
});

// Change the 404 message modifing the middleware
app.use(function(req, res, next) {
    res.status(404).send("Sorry, that route doesn't exist. Have a nice day :)");
});

var port = process.env.port || 5000;
app.listen(port, () => console.log(`Example app listening to requests on port ${port}!`))
